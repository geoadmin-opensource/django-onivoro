=====
Usage
=====

To use Django Onivoro in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'onivoro.apps.OnivoroConfig',
        ...
    )

Add Django Onivoro's URL patterns:

.. code-block:: python

    from onivoro import urls as onivoro_urls


    urlpatterns = [
        ...
        url(r'^', include(onivoro_urls)),
        ...
    ]
