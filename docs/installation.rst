============
Installation
============

At the command line::

    $ easy_install django-onivoro

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-onivoro
    $ pip install django-onivoro
