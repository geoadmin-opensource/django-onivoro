# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-17 23:13
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FonteExterna',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('app', models.CharField(help_text='Aplica\xe7\xe3o Django que cont\xe9m o modelo em quest\xe3o', max_length=128, verbose_name='Aplica\xe7\xe3o Django')),
                ('modelo', models.CharField(help_text='Modelo Django que ser\xe1 utilizado.', max_length=128, verbose_name='Modelo Django')),
                ('criado_em', models.DateTimeField(auto_now_add=True, help_text='Data de cria\xe7\xe3o da fonte externa.', verbose_name='Criado em')),
                ('atualizado_em', models.DateTimeField(auto_now=True, help_text='Data da \xfaltima atualiza\xe7\xe3o desta fonte externa.', verbose_name='Atualizado em')),
                ('nome', models.CharField(help_text='Texto identificador da fonte externa.', max_length=128, unique=True, verbose_name='Nome')),
                ('descricao', models.CharField(help_text='Texto descritivo da fonte externa', max_length=512, verbose_name='Descri\xe7\xe3o da Fonte Externa')),
                ('srid', models.IntegerField(default=4326, help_text='SRID do sistema de refer\xeancia de entrada.', verbose_name='SRID (nativo)')),
                ('encoding', models.CharField(default=b'latin-1', help_text='Codifica\xe7\xe3o em que o dado original se encontra. Ex: utf-8, latin-1, etc. Padr\xe3o \xe9 latin-1.', max_length=16, verbose_name='Codifica\xe7\xe3o')),
                ('tipo', models.CharField(choices=[('SHAPEFILE', 'Shapefile'), ('GEOJSON', 'GeoJson'), ('WFS', 'WFS (Web Feature Service)')], max_length=32, verbose_name='Tipo da Fonte Externa')),
                ('url_sincronia', models.URLField(help_text='URL do recurso a ser sincronizado', verbose_name='Endere\xe7o Sincronia')),
                ('formato', models.CharField(choices=[('SHP', 'Shapefile'), ('JSON', 'GeoJson'), ('ZIP', 'Zip'), ('TAR.GZ', 'Tarball'), ('REQUEST', 'Request HTTP')], help_text='Formato do arquivo utilizado para sincronzia\xe7\xe3o.', max_length=6, verbose_name='Formato Arquivo')),
                ('ultima_sincronizacao', models.DateTimeField(help_text='Data em que o modelo foi sincronizado pela \xfaltima vez.', verbose_name='\xdaltima sincroniza\xe7\xe3o')),
                ('nome_alvo', models.CharField(blank=True, help_text='Nome do arquivo alvo. S\xf3 \xe9 necess\xe1rio se um zip cont\xe9m mais de um arquivo do mesmo tipo.', max_length=128, null=True, verbose_name='Nome do Arquivo Alvo')),
            ],
            options={
                'ordering': ('-atualizado_em',),
                'verbose_name': 'Fonte Externa',
                'verbose_name_plural': 'Fontes Externas',
            },
        ),
        migrations.CreateModel(
            name='Mapeamento',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('campo_origem', models.CharField(help_text='Nome do campo, na origem (fonte externa).', max_length=64, verbose_name='Nome do Campo (fonte externa)')),
                ('campo_destino', models.CharField(help_text='Nome do campo, no destino (modelo GeoDjango).', max_length=64, verbose_name='Nome do Campo (modelo GeoDjango)')),
                ('campo_destino_estrangeira', models.CharField(blank=True, help_text='Chave estrangeira do modelo.', max_length=64, null=True, verbose_name='Nome do Campo Chave Estrangeira (modelo GeoDjango)')),
                ('tipo_origem', models.CharField(choices=[(b'GEO', 'Geometria'), (b'INT', 'Inteiro'), (b'FLOAT', 'Decimal'), (b'STRING', 'Texto'), (b'DATA', 'Data'), (b'DATA-HORA', 'Data/Hora'), (b'FK', 'Chave Estrangeira')], editable=False, help_text='Tipo do campo, na origem (fonte externa).', max_length=32, verbose_name='Tipo do Campo (fonte externa)')),
                ('tipo_destino', models.CharField(choices=[(b'GEO', 'Geometria'), (b'INT', 'Inteiro'), (b'FLOAT', 'Decimal'), (b'STRING', 'Texto'), (b'DATA', 'Data'), (b'DATA-HORA', 'Data/Hora'), (b'FK', 'Chave Estrangeira')], editable=False, help_text='Tipo do Campo, no destino (modelo GeoDjango).', max_length=32, verbose_name='Tipo do Campo (modelo GeoDjango)')),
                ('fonte_externa', models.ForeignKey(help_text='Fonte externa em que este mapeamento de campos est\xe1 associado.', on_delete=django.db.models.deletion.CASCADE, related_name='mapeamentos', to='onivoro.FonteExterna', verbose_name='Fonte Externa')),
            ],
            options={
                'verbose_name': 'Mapeamento de Campo',
                'verbose_name_plural': 'Mapeamento de Campos',
            },
        ),
        migrations.CreateModel(
            name='Transformacao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(help_text='Nome da Transforma\xe7\xe3o', max_length=256, unique=True, verbose_name='Nome')),
                ('descricao', models.CharField(help_text='Descri\xe7\xe3o da Transforma\xe7\xe3o.', max_length=256, verbose_name='Descri\xe7\xe3o')),
                ('tipo_campo', models.CharField(choices=[(b'GEO', 'Geometria'), (b'INT', 'Inteiro'), (b'FLOAT', 'Decimal'), (b'STRING', 'Texto'), (b'DATA', 'Data'), (b'DATA-HORA', 'Data/Hora'), (b'FK', 'Chave Estrangeira')], help_text='Tipo de Campo a qual esta transforma\xe7\xe3o se aplica.', max_length=32, verbose_name='Tipo de Campo Aplic\xe1vel')),
                ('metodo_conversao', models.TextField(help_text='C\xf3digo que ser\xe1 utilizado para transformar os valores. A entrada dever\xe1 ser obrigat\xf3riamente uma fun\xe7\xe3o com um argumento e exatamente um retorno.', verbose_name='C\xf3digo utilizado na transforma\xe7\xe3o')),
            ],
            options={
                'verbose_name': 'Transforma\xe7\xe3o',
                'verbose_name_plural': 'Transforma\xe7\xf5es',
            },
        ),
        migrations.AddField(
            model_name='mapeamento',
            name='transformacoes',
            field=models.ManyToManyField(help_text='Transforma\xe7\xf5es ser\xe3o executadas para cada campo, de forma a adequar o mesmo ao modelo interno.', to='onivoro.Transformacao', verbose_name='Transforma\xe7\xf5es'),
        ),
        migrations.AlterUniqueTogether(
            name='mapeamento',
            unique_together=set([('fonte_externa', 'campo_destino')]),
        ),
    ]
