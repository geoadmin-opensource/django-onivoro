# -*- coding: utf-8
from django.apps import AppConfig


class OnivoroConfig(AppConfig):
    name = 'onivoro'
