# -*- coding: utf-8
from __future__ import unicode_literals, absolute_import

from django.conf.urls import url, include

from onivoro.urls import urlpatterns as onivoro_urls

urlpatterns = [
    url(r'^', include(onivoro_urls, namespace='onivoro')),
]
