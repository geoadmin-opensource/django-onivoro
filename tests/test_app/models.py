# coding: utf-8
"""modulo com modelos mock"""
from django.contrib.gis.db import models


class UnidadeConservacao(models.Model):

    nome = models.CharField(max_length=256,
                            verbose_name=u"Nome",
                            help_text=u"Nome da Unidade de Conservação")

    # replicado para chave primária ID
    id_mma = models.CharField(max_length=30,
                              verbose_name=u"Identificador (MMA)",
                              help_text=u"Número identificador (MMA).",
                              null=True,
                              blank=True,
                              db_index=True)

    def __unicode__(self):
        """
        Retorna a representação de UC
        """
        return self.nome

    geometria = models.MultiPolygonField(srid=4674)

    objects = models.GeoManager()

    class Meta:
        verbose_name = u"Unidade de Conservação"
        verbose_name_plural = u"Unidades de Conservação"
