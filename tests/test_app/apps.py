from django.apps import AppConfig

class FakeOnivoroConfig(AppConfig):
    name = 'fake_onivoro'
    verbose_name = "Fake Onivoro"