# coding: utf-8
from django.contrib.gis.gdal.field import OFTInteger, OFTWideString, OFTDateTime, OFTDate, OFTReal, OFTString, OFTIntegerList, OFTWideStringList, OFTStringList, OFTRealList, OFTBinary, OFTTime
from django.test import TestCase
from onivoro.ogr_utils import ConversorTipoCampo

class ConversorTipoCampoTestCase(TestCase):
    """
    OGRFieldTypes = { 0 : OFTInteger,
                  1 : OFTIntegerList,
                  2 : OFTReal,
                  3 : OFTRealList,
                  4 : OFTString,
                  5 : OFTStringList,
                  6 : OFTWideString,
                  7 : OFTWideStringList,
                  8 : OFTBinary,
                  9 : OFTDate,
                  10 : OFTTime,
                  11 : OFTDateTime,
                  }
    """

    def tearDown(self):
        pass

    def setUp(self):
        pass

    def test_tipo_coindice_OFTString_STRING(self):
        """
        Testa se os tipos OFString e String coincidem
        """
        self.assertTrue(ConversorTipoCampo.tipo_coincide(OFTString,"STRING"))

    def test_tipo_coindice_OFTInteger_INT(self):
        """
        Testa se os tipos OFString e String coincidem
        """
        self.assertTrue(ConversorTipoCampo.tipo_coincide(OFTInteger,"INT"))

    def test_tipo_coindice_OFTReal_FLOAT(self):
        """
        Testa se os tipos OFString e String coincidem
        """
        self.assertTrue(ConversorTipoCampo.tipo_coincide(OFTReal,"FLOAT"))

    def test_tipo_coindice_OFTWideString_STRING(self):
        """
        Testa se os tipos OFString e String coincidem
        """
        self.assertTrue(ConversorTipoCampo.tipo_coincide(OFTWideString,"STRING"))

    def test_tipo_coindice_OFTDate_DATA(self):
        """
        Testa se os tipos OFString e String coincidem
        """
        self.assertTrue(ConversorTipoCampo.tipo_coincide(OFTDate,"DATA"))

    def test_tipo_coindice_OFTDateTime_DATAHORA(self):
        """
        Testa se os tipos OFString e String coincidem
        """
        self.assertTrue(ConversorTipoCampo.tipo_coincide(OFTDateTime,"DATA-HORA"))

    def test_fk_coincide_com_tudo(self):
        self.assertTrue(ConversorTipoCampo.tipo_coincide(OFTInteger,"FK"))
        self.assertTrue(ConversorTipoCampo.tipo_coincide(OFTReal,"FK"))
        self.assertTrue(ConversorTipoCampo.tipo_coincide(OFTString,"FK"))
        self.assertTrue(ConversorTipoCampo.tipo_coincide(OFTWideString,"FK"))
        self.assertTrue(ConversorTipoCampo.tipo_coincide(OFTDate,"FK"))
        self.assertTrue(ConversorTipoCampo.tipo_coincide(OFTDateTime,"FK"))

    def test_tipo_nao_coindice_OFTList(self):
        """
        Testa que qualquer campo do tipo lista não coincide com nada que temos.
        """
        self.assertFalse(ConversorTipoCampo.tipo_coincide(OFTIntegerList,"FK"))
        self.assertFalse(ConversorTipoCampo.tipo_coincide(OFTRealList,"FK"))
        self.assertFalse(ConversorTipoCampo.tipo_coincide(OFTStringList,"FK"))
        self.assertFalse(ConversorTipoCampo.tipo_coincide(OFTWideStringList,"FK"))
        self.assertFalse(ConversorTipoCampo.tipo_coincide(OFTBinary,"FK"))
        self.assertFalse(ConversorTipoCampo.tipo_coincide(OFTTime,"FK"))
