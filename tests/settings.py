# -*- coding: utf-8
from __future__ import unicode_literals, absolute_import
import os
import django

DEBUG = True
USE_TZ = True
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "(kkhx*&y(4e%5f!6zgk-3+p6d&2q3!@$o&tm^i@5ig_x9c3pk_"
ONIVORO_MAIL_NOTIFY = True
ONIVORO_MAIL_RECIPIENTS = ["george@sigmageosistemas.com.br"]
ONIVORO_MAIL_SENDER = "george@sigmageosistemas.com.br"
DATABASES = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "USER": "postgres",
        "PASSWORD": "postgres",
        "HOST": "localhost",
        "PORT": "5432"
    }
}

ROOT_URLCONF = "tests.urls"

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sites",
    "onivoro",
    "tests.test_app",
]

SITE_ID = 1

if django.VERSION >= (1, 10):
    MIDDLEWARE = ()
else:
    MIDDLEWARE_CLASSES = ()
