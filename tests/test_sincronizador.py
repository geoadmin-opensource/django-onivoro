# coding: utf-8
import shutil
import os
from urllib2 import HTTPError
from django.conf import settings
from django.test import TestCase
from onivoro.models import FonteExterna
from onivoro.sincronizador import Sincronizador
from onivoro.exceptions import SincronizadorException
from test_app.models import UnidadeConservacao
from .mocks import (
    mockFonteExternaShapefileAvulso,
    mockFonteExternaShapefileZipped,
    mockFonteExternaCompletaUC,
    mockFonteExternaJsonZipped
)


class SincronizadorTestCase(TestCase):

    TEMP_DATA_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "temp_test_data")
    TEST_DATA_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "test_data")

    def setUp(self):
        settings.ONIVORO_DATA_ROOT = self.TEMP_DATA_DIR
        if os.path.exists(self.TEMP_DATA_DIR):
            shutil.rmtree(self.TEMP_DATA_DIR)

        shutil.copytree(self.TEST_DATA_DIR, self.TEMP_DATA_DIR)

    def tearDown(self):
        FonteExterna.objects.all().delete()
        shutil.rmtree(self.TEMP_DATA_DIR)

    def test_download_shapefile_solto(self):
        """
        Teste para assegurar que o download está sendo feito corretamente.
        Este teste cuida do shapefile solto na web, buscando os arquivos .dbf e .shhx correspondentes.
        """
        try:
            fe = mockFonteExternaShapefileAvulso()
            sync = Sincronizador(fe)
            sync._download()
            # baixamos todos os arquivos?
            arquivos = os.listdir(self.TEMP_DATA_DIR)
            self.assertTrue("ucsmi.shp" in arquivos)
            self.assertTrue("ucsmi.shx" in arquivos)
            self.assertTrue("ucsmi.dbf" in arquivos)
        except HTTPError:
            self.fail("Este teste deveria passar mas o recurso http não está disponível. Tente mais tarde.")

    def test_descompactar_zip_dois_shps(self):
        """
        Teste para assegurar que um zip com dois shps não será aceito.
        """
        fe = mockFonteExternaShapefileZipped()
        sync = Sincronizador(fe)

        sync.arquivo_v0 = os.path.join(self.TEMP_DATA_DIR, "ucsmu_dois_shps.zip")
        self.assertRaises(SincronizadorException, sync._descompactar)

    def test_descompactar_tar_dois_shps(self):
        """
        Teste para assegurar que um tar com dois shps não será aceito.
        """
        fe = mockFonteExternaShapefileZipped()
        sync = Sincronizador(fe)

        sync.arquivo_v0 = os.path.join(self.TEMP_DATA_DIR, "ucsmu_dois_shps.tar.gz")
        self.assertRaises(SincronizadorException, sync._descompactar)

    def test_descompactar_zip_dois_jsons(self):
        """
        Teste para assegurar que um zip com dois jsons não será aceito.
        """
        fe = mockFonteExternaJsonZipped()
        sync = Sincronizador(fe)

        sync.arquivo_v0 = os.path.join(self.TEMP_DATA_DIR, "vazio_dois_json.zip")
        self.assertRaises(SincronizadorException, sync._descompactar)

    def test_descompactar_tar_dois_jsons(self):
        """
        Teste para assegurar que um tar com dois jsons não será aceito.
        """
        fe = mockFonteExternaJsonZipped()
        sync = Sincronizador(fe)

        sync.arquivo_v0 = os.path.join(self.TEMP_DATA_DIR, "vazio_dois_json.tar.gz")
        self.assertRaises(SincronizadorException, sync._descompactar)

    def test_descompactar_tar_correto(self):
        """
        Teste para descompactação de um arquivo .zip completo
        com todos os arquivos requeridos de shp.
        """
        fe = mockFonteExternaShapefileZipped()
        sync = Sincronizador(fe)

        sync.arquivo_v0 = os.path.join(self.TEMP_DATA_DIR, "ucsmu_correto.tar.gz")
        sync._descompactar()

        arquivos = os.listdir(os.path.join(self.TEMP_DATA_DIR, "ucsmu_correto"))

        self.assertEqual(os.path.split(sync.arquivo_alvo)[-1], "ucsmu.shp")
        self.assertTrue("ucsmu.shp" in arquivos)
        self.assertTrue("ucsmu.shx" in arquivos)
        self.assertTrue("ucsmu.dbf" in arquivos)

    def test_descompactar_tar_sem_shp(self):
        """
        Testa que sem um arquivo .shp a descompactacao quebra!
        """
        fe = mockFonteExternaShapefileZipped()
        sync = Sincronizador(fe)

        sync.arquivo_v0 = os.path.join(self.TEMP_DATA_DIR, "ucsmu_miss_shp.tar.gz")
        self.assertRaises(SincronizadorException, sync._descompactar)

    def test_descompactar_tar_sem_shx(self):
        """
        Testa que sem um arquivo .shp a descompactacao quebra!
        """
        fe = mockFonteExternaShapefileZipped()
        sync = Sincronizador(fe)

        sync.arquivo_v0 = os.path.join(self.TEMP_DATA_DIR, "ucsmu_miss_shx.tar.gz")
        self.assertRaises(SincronizadorException, sync._descompactar)

    def test_descompactar_tar_sem_dbf(self):
        """
        Testa que sem um arquivo .shp a descompactacao quebra!
        """
        fe = mockFonteExternaShapefileZipped()
        sync = Sincronizador(fe)

        sync.arquivo_v0 = os.path.join(self.TEMP_DATA_DIR, "ucsmu_miss_dbf.tar.gz")
        self.assertRaises(SincronizadorException, sync._descompactar)

    def test_descompactar_zip_correto(self):
        """
        Teste para descompactação de um arquivo .zip completo
        com todos os arquivos requeridos de shp.
        """
        fe = mockFonteExternaShapefileZipped()
        sync = Sincronizador(fe)

        sync.arquivo_v0 = os.path.join(self.TEMP_DATA_DIR, "ucsmu_correto.zip")
        sync._descompactar()

        arquivos = os.listdir(os.path.join(self.TEMP_DATA_DIR, "ucsmu_correto"))

        self.assertEqual(os.path.split(sync.arquivo_alvo)[-1], "ucsmu.shp")
        self.assertTrue("ucsmu.shp" in arquivos)
        self.assertTrue("ucsmu.shx" in arquivos)
        self.assertTrue("ucsmu.dbf" in arquivos)

    def test_descompactar_zip_sem_shp(self):
        """
        Testa que sem um arquivo .shp a descompactacao quebra!
        """
        fe = mockFonteExternaShapefileZipped()
        sync = Sincronizador(fe)

        sync.arquivo_v0 = os.path.join(self.TEMP_DATA_DIR, "ucsmu_miss_shp.zip")
        self.assertRaises(SincronizadorException, sync._descompactar)

    def test_descompactar_zip_sem_shx(self):
        """
        Testa que sem um arquivo .shp a descompactacao quebra!
        """
        fe = mockFonteExternaShapefileZipped()
        sync = Sincronizador(fe)

        sync.arquivo_v0 = os.path.join(self.TEMP_DATA_DIR, "ucsmu_miss_shx.zip")
        self.assertRaises(SincronizadorException, sync._descompactar)

    def test_descompactar_zip_sem_dbf(self):
        """
        Testa que sem um arquivo .shp a descompactacao quebra!
        """
        fe = mockFonteExternaShapefileZipped()
        sync = Sincronizador(fe)

        sync.arquivo_v0 = os.path.join(self.TEMP_DATA_DIR, "ucsmu_miss_dbf.zip")
        self.assertRaises(SincronizadorException, sync._descompactar)

    def test_sincronizador_completo_shapefile_avulso(self):
        """
        Teste completo de download e carga de um shapefile
        """
        try:
            fe = mockFonteExternaCompletaUC()
            sync = Sincronizador(fe)
            sync.sincronizar()
            self.assertGreater(UnidadeConservacao.objects.all().count(), 0)
        except HTTPError:
            self.fail("Este teste deveria passar mas o recurso http não está disponível. Tente mais tarde.")

    def test_sincronizar_shape_projecao(self):
        """
        Este teste está aqui para garantir que o sistema irá converter
        de uma projeção para nossa projeção default caso a entrada tenha informações
        de projeção.
        Exemplo:
        entrada em SAD69 -> importacao -> saida em SIRGAS 2000.
        """
        pass
        # self.fail()

    def test_sincronizar_shape_projecao_invalida(self):
        pass
        # self.fail()

    def test_sincronizar_shape_sem_projecao(self):
        pass
        # self.fail()

    def test_validar_campos(self):
        """
        Este teste está aqui para garantir que uma etapa de validação
        entre os campos do shapefile e do modelo interno será executado.
        Se tudo ok, os dados devem ser importados (veja test_sincronizador_completo_shapefile).
        """
        fe = mockFonteExternaCompletaUC()
        sync = Sincronizador(fe)
        sync.arquivo_v0 = os.path.join(self.TEMP_DATA_DIR, "ucsmu_correto.zip")
        sync._descompactar()
        erros = sync._validar_campos()

        self.assertEqual(len(erros), 0)

    def test_validar_campos_origem_diferente(self):
        """
        Teste para garantir que o sistema dará o grito quando a fonte dos dados mudar
        por algum motivo, sugerindo ao usuário REMAPEAR os campos.
        """
        fe = mockFonteExternaCompletaUC()
        sync = Sincronizador(fe)
        sync.arquivo_v0 = os.path.join(self.TEMP_DATA_DIR, "ucsmu_sem_campo_iduc0.zip")
        sync._descompactar()
        erros = sync._validar_campos()

        self.assertEqual(len(erros), 1)
        self.assertRaises(SincronizadorException, sync._carrega)

    def test_sincronizador_404(self):
        """
        Teste de sincronia completo. Esperamos que OCORRA uma exceção 404!
        """
        fe = mockFonteExternaCompletaUC()
        fe.url_sincronia = "http://google.com/foo"
        sync = Sincronizador(fe)
        self.assertRaises(HTTPError, sync._download)
