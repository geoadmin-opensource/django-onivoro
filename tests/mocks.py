# coding: utf-8
from django.contrib.auth.models import User
from django.utils.timezone import now
from onivoro.models import FonteExterna, Transformacao, Mapeamento


def mockFonteExternaShapefileAvulso():
    """
    Cria uma fonte externa apontando para uma URL do MMA.
    Somente para teste!
    """
    f = FonteExterna()
    f.criado_em = now()
    f.atualizado_em = now()

    # este nome está com a data só para facilitar a vida nos testes :D
    f.nome = "Unidade de Conservação Municipal %s" % str(now())

    f.descricao = "fonte externa!"
    f.tipo = "SHAPEFILE"
    f.app = "test_app"
    f.modelo = "UnidadeConservacao"

    # este cara está utilizando a ucs municipais pelo tamanho reduzido da mesma (580kb)
    f.url_sincronia = "http://mapas.mma.gov.br/ms_tmp/ucsmi.shp"

    f.ultima_sincronizacao = now()
    f.formato = "SHP"
    f.save()
    return f


def mockFonteExternaJsonZipped():
    f = FonteExterna()
    f.criado_em = now()
    f.atualizado_em = now()
    f.nome = "teste_fonte_externa"
    f.descricao = "fonte externa!"
    f.tipo = "GEOJSON"
    f.app = "test_app"
    f.modelo = "UnidadeConservacao"

    # todo: descobrir um arquivo .json na web com dados gis!
    f.url_sincronia = ""

    f.ultima_sincronizacao = now()
    f.formato = "ZIP"
    f.save()
    return f


def mockFonteExternaShapefileZipped():
    """
    Cria uma fonte externa apontando para uma URL aleatória com uns dados pequenos.
    http://www.direitominerario.com/downloads.htm
    Somente para propósitos de teste.
    """
    f = FonteExterna()
    f.criado_em = now()
    f.atualizado_em = now()
    f.nome = "teste_fonte_externa"
    f.descricao = "fonte externa!"
    f.tipo = "SHAPEFILE"
    f.app = "test_app"
    f.modelo = "UnidadeConservacao"

    # tamanho pequeno!
    f.url_sincronia = "http://www.direitominerario.com/data/Usinas%20eolicas%20BR.zip"

    f.ultima_sincronizacao = now()
    f.formato = "ZIP"
    f.save()
    return f


def mockFonteExternaCompletaUC():
    f = mockFonteExternaShapefileAvulso()
    nome_map = Mapeamento()
    nome_map.campo_origem = "NOME_UC1"
    nome_map.campo_destino = "nome"
    nome_map.tipo_origem = "STRING"
    nome_map.tipo_destino = "STRING"
    nome_map.fonte_externa = f
    nome_map.save()
    idmma_map = Mapeamento()
    idmma_map.campo_origem = "ID_UC0"
    idmma_map.campo_destino = "id_mma"
    idmma_map.tipo_origem = "STRING"
    idmma_map.tipo_destino = "STRING"
    idmma_map.fonte_externa = f
    idmma_map.save()
    geom_map = Mapeamento()
    geom_map.campo_origem = "POLYGON"
    geom_map.campo_destino = "geometria"
    geom_map.tipo_origem = "GEO"
    geom_map.tipo_destino = "GEO"
    geom_map.fonte_externa = f
    geom_map.save()

    return f


def mockTransformacao():
    """
    Cria uma transformação coxa e devolve por aí.
    Somente para teste.
    """
    trans = Transformacao()
    trans.nome = "Transformação Tamanho String"
    trans.descricao = "Reduz o tamanho de uma string para o tamanho adequado."
    trans.tipo_campo = "STRING"
    trans.metodo_conversao = """
# coding: utf-8
def converte(entrada,campo_novo):
    max = campo_novo.max_length

    if len(entrada) > max:
        saida = entrada[:max]
    else:
        saida = entrada

    return saida

valor_novo = converte(entrada,campo_novo)
"""
    trans.save()
    return trans


def mockUser():
    u = User(username="george", password="george")
    u.save()
    return u


def mockMapeamento():
    map = Mapeamento()
    map.fonte_externa = mockFonteExternaShapefileAvulso()
    map.campo_origem = "ID_UC0"
    map.campo_destino = "id_mma"
    map.tipo_origem = "STRING"
    map.tipo_destino = "STRING"
    map.save()
    return map
