# coding: utf-8
from django.contrib.auth.models import User
from django.test import TestCase
from onivoro.models import Mapeamento, Transformacao, FonteExterna
from onivoro.exceptions import TransformacaoException
from django.db import models
from .mocks import *


class FonteExternaTestCase(TestCase):
    """
    Testes específicos para o modelo FonteExterna
    """

    def tearDown(self):
        Mapeamento.objects.all().delete()
        FonteExterna.objects.all().delete()

    def test_mapa_campos(self):
        """
        Teste para buscar o mapa de campos de uma fonte externa.
        """
        fe = mockFonteExternaCompletaUC()

        self.assertIsNotNone(fe.mapa_campos)
        self.assertEqual(len(fe.mapeamentos.all()), 3)
        self.assertEqual(len(fe.mapa_campos), 3)
        self.assertDictEqual(fe.mapa_campos, {"nome": "NOME_UC1",
                                              "id_mma": "ID_UC0",
                                              "geometria": "POLYGON"})

    def test_mapa_campos_complexo(self):
        """
        Teste para buscar o mapa de campos de uma fonte externa, com chaves estrangeiras
        """
        fe = mockFonteExternaCompletaUC()
        mapa_fk = Mapeamento()
        mapa_fk.campo_origem = "DECRETO"
        mapa_fk.campo_destino = "id_decreto"
        mapa_fk.tipo_origem = "STRING"
        mapa_fk.tipo_destino = "FK"
        mapa_fk.campo_destino_estrangeira = "id"
        mapa_fk.fonte_externa = fe
        mapa_fk.save()

        self.assertIsNotNone(fe.mapa_campos)
        self.assertEqual(len(fe.mapeamentos.all()), 4)
        self.assertEqual(len(fe.mapa_campos), 4)
        self.assertDictEqual(fe.mapa_campos, {"nome": "NOME_UC1",
                                              "id_mma": "ID_UC0",
                                              "geometria": "POLYGON",
                                              "id_decreto": {"id": "DECRETO"}})

    def test_modelo_geo(self):
        """
        Teste para assegurar que é possível buscar um modelo geo a partir da
        fonte externa
        """

        fe = mockFonteExternaCompletaUC()
        model = fe.ref_modelo()
        self.assertIsNotNone(model)

    def test_url_valida_wfs(self):
        # self.fail()
        pass

    def test_url_valida_shape(self):
        """
        Teste para garantir que uma url do tipo SHP é valida.
        """
        # self.fail()
        pass

    def test_url_valida_geojson(self):
        """
        """
        pass
        # self.fail()

    def test_get_absolute_url(self):
        pass
        # self.fail()


class TransformacaoTestCase(TestCase):
    """
    Testes para o modelo de Transformacao
    """

    def setUp(self):
        pass

    def tearDown(self):
        Transformacao.objects.all().delete()

    def test_criacao_transformacao(self):
        """
        Teste bobo para salvar uma transformação.
        """
        trans = Transformacao()
        trans.nome = "Transformação Tamanho String"
        trans.descricao = "Reduz o tamanho de uma string para o tamanho adequado."
        trans.tipo_campo = "STRING"
        trans.metodo_conversao = """
def converter(valor_antigo,campo_novo):
    max = campo_novo._meta.max_length

    if len(valor_antigo) > max:
        valor_novo = valor_antigo[:max]
    else:
        return valor_novo
        """
        trans.save()

    def test_codigo_inofensivo(self):
        trans = mockTransformacao()
        self.assertFalse(trans.codigo_perigoso())

    def test_codigo_ofensivo_while(self):
        trans = mockTransformacao()
        trans.metodo_conversao = """
def converter(valor_antigo,campo_novo):
    while 1:
        print "treta"
        """
        self.assertTrue(trans.codigo_perigoso())

    def test_codigo_ofensivo_import(self):
        trans = mockTransformacao()
        trans.metodo_conversao = """
import sys
def converter(valor_antigo,campo_novo):
    pass
        """
        self.assertTrue(trans.codigo_perigoso())

    def test_codigo_ofensivo_import2(self):
        trans = mockTransformacao()
        trans.metodo_conversao = """
from os import path
def converter(valor_antigo,campo_novo):
    pass
        """
        self.assertTrue(trans.codigo_perigoso())

    def test_codigo_ofensivo_exec(self):
        trans = mockTransformacao()
        trans.metodo_conversao = """
def converter(valor_antigo,campo_novo):
    exec 'valor_antigo += 1'
        """
        self.assertTrue(trans.codigo_perigoso())

    def test_codigo_ofensivo_eval(self):
        trans = mockTransformacao()
        trans.metodo_conversao = """
def converter(valor_antigo,campo_novo):
    eval '1 + 1'
        """
        self.assertTrue(trans.codigo_perigoso())

    def test_codigo_incorreto(self):
        """
        Testa que um código com sintaxe inválida não poderá ser salvo pelo sistema.
        """
        trans = mockTransformacao()
        trans.metodo_conversao = """
def converter(valor_antigo,campo_novo):
    if abc
        return "abc"
"""
        self.assertRaises(TransformacaoException, trans.save)

    def test_execucao_codigo_usuario(self):
        trans = mockTransformacao()
        stub_campo_novo = models.CharField(max_length=6)
        retorno = trans.transforma("blablabla", stub_campo_novo)
        self.assertEqual(retorno, "blabla")


class MapeamentoTestCase(TestCase):
    """
    Test case para o modelo Mapeamento (de campos)
    """

    def tearDown(self):
        Mapeamento.objects.all().delete()
        FonteExterna.objects.all().delete()
        User.objects.all().delete()

    def test_validar_conversao_mesmo_tipo(self):
        """
        Assegura que as conversões entre mesmo tipo
        são válidas.
        """
        map = Mapeamento()
        map.tipo_origem = "STRING"
        map.tipo_destino = "STRING"
        self.assertTrue(map.conversao_valida())

        map.tipo_origem = "INT"
        map.tipo_destino = "INT"
        self.assertTrue(map.conversao_valida())

        map.tipo_origem = "FLOAT"
        map.tipo_destino = "FLOAT"
        self.assertTrue(map.conversao_valida())

        map.tipo_origem = "GEO"
        map.tipo_destino = "GEO"
        self.assertTrue(map.conversao_valida())

        map.tipo_origem = "DATA"
        map.tipo_destino = "DATA"
        self.assertTrue(map.conversao_valida())

        map.tipo_origem = "DATA-HORA"
        map.tipo_destino = "DATA-HORA"
        self.assertTrue(map.conversao_valida())

    def test_validar_campo_fk(self):
        """
        Teste que garante que um tipo FK com campo estrangeiro está ok!
        """
        map = Mapeamento()
        map.tipo_origem = "STRING"
        map.tipo_destino = "FK"
        map.campo_destino_estrangeira = "nome"
        self.assertTrue(map.conversao_valida())

    def test_validar_campo_fk_nao_permitida(self):
        """
        Teste que garante que todo tipo de destino FK tenha um
        campo estrangeiro!
        """
        map = Mapeamento()
        map.tipo_origem = "STRING"
        map.tipo_destino = "FK"
        self.assertFalse(map.conversao_valida())

    def test_validar_conversao_int_float(self):
        """
        Testa que as conversões entre float e int são consideradas válidas.
        """

        map = Mapeamento()
        map.tipo_origem = "INT"
        map.tipo_destino = "FLOAT"
        self.assertTrue(map.conversao_valida())

        map.tipo_origem = "FLOAT"
        map.tipo_destino = "INT"
        self.assertTrue(map.conversao_valida())

    def test_validar_conversao_outros_string(self):
        """
        Testa que as conversões entre outros tipos e string são válidas.
        """

        map = Mapeamento()
        map.tipo_origem = "INT"
        map.tipo_destino = "STRING"
        self.assertTrue(map.conversao_valida())

        map.tipo_origem = "FLOAT"
        map.tipo_destino = "STRING"
        self.assertTrue(map.conversao_valida())

        map.tipo_origem = "DATA"
        map.tipo_destino = "STRING"
        self.assertTrue(map.conversao_valida())

        map.tipo_origem = "DATA-HORA"
        map.tipo_destino = "STRING"
        self.assertTrue(map.conversao_valida())

    def test_validar_conversao_string_outros(self):
        """
        Garante que as conversoes entre os tipos abaixo
        são invalidas
        """
        map = Mapeamento()
        map.tipo_origem = "STRING"
        map.tipo_destino = "INT"
        self.assertFalse(map.conversao_valida())

        map.tipo_origem = "STRING"
        map.tipo_destino = "FLOAT"
        self.assertFalse(map.conversao_valida())

        map.tipo_origem = "STRING"
        map.tipo_destino = "DATA"
        self.assertFalse(map.conversao_valida())

        map.tipo_origem = "STRING"
        map.tipo_destino = "DATA-HORA"
        self.assertFalse(map.conversao_valida())

        map.tipo_origem = "STRING"
        map.tipo_destino = "GEO"
        self.assertFalse(map.conversao_valida())

